"""
Rules-definitions required by cenv.

Information
-----------
* authors:
    * Simon Kallfass
* python:
    * 3.6
"""


from pathlib import Path

import attr


@attr.s(slots=True)
class CondaCmdFormats:
    """
    Class containing the formats for the conda commands to use inside cenv.
    """
    remove = '{conda} remove -n {name} --all'
    activate = '{conda} activate {name}'
    export = '{conda} env export -n {name} > environment.yml'
    create = '{conda} create -n {name} {pkgs}'
    clone = '{conda} create -n {name}_backup --clone {name}'
    restore = '{conda} create -n {name} --clone {name}_backup'
    clean = '{conda} remove -n {name}_backup --all'

    def conda_bin(self, conda_folder):
        """
        Combines the path of installation-folder of conda to gain the
        conda-executable.

        Returns
        -------
        conda_path
            the path to the conda-executable
        """
        conda_path = (conda_folder / 'bin/conda').absolute()
        return conda_path


@attr.s(slots=True)
class Rules:
    """
    Rules required by cenv.
    """
    conda_cmds = CondaCmdFormats()
    git_folder = '.git'


RULES = Rules()
