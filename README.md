# conda-env-manager (cenv)

**Important:**
>    Conda should be installed at **/opt/conda** using debian respository as
>    described at
>    [debian repositories for miniconda](
>    https://www.anaconda.com/rpm-and-debian-repositories-for-miniconda/).
>
>    The path for the cached-packages should be **/shared/conda/pkgs**.
>
>    The path to store the conda-environments should be **/shared/conda/envs**.


## about conda
* from Anaconda
* Packagemanager
* Environmentmanager => Portability and robust code
* Part of Anaconda (Python Distribution with focus on scientific work and
  data analysis)
* Allows good dependency solving (not only python in contrast to pip)
* includes own pip
* includes own build-system
* allows usage of own repository-channels => independency and own
  conda-packages
* two versions: Miniconda and Anaconda (we use miniconda)


**Definition:**
>    Conda is an open source package management system and environment management
>    system that runs on Windows, macOS and Linux. Conda quickly installs, runs
>    and updates packages and their dependencies. Conda easily creates, saves,
>    loads and switches between environments on your local computer. It was
>    created for Python programs, but it can package and distribute software for
>    any language.
>
>    Conda as a package manager helps you find and install packages. If you need
>    a package that requires a different version of Python, you do not need to
>    switch to a different environment manager, because conda is also an
>    environment manager. With just a few commands, you can set up a totally
>    separate environment to run that different version of Python, while continuing
>    to run your usual version of Python in your normal environment.
>    [...]
>    The conda package and environment manager is included in all versions of
>    Anaconda®, Miniconda and Anaconda Repository.[...]
>
>    (as defined at https://docs.conda.io)


**Helpful links:**
>    * [conda documentation](https://conda.io/projects/conda/en/latest/index.html)
>      more detailed understanding for conda
>    * [Anaconda blog](https://www.anaconda.com/blog/) to stay up to date for
>      changes in conda and other news concerning conda and anaconda


## installation
Install conda as described at
[debian repositories for miniconda](https://www.anaconda.com/rpm-and-debian-repositories-for-miniconda/).

Create the folder **/shared/conda**.

Perhaps you'll have to change the permissions of the folders.

Place the following **.condarc** at **/opt/conda/.condarc**:
```yaml
allow_other_channels: True
show_channel_urls: True

pkgs_dirs:
  - /shared/conda/pkgs

envs_dirs:
  - /shared/conda/envs

channels:
  - defaults
  - local
  - https://www.repository-channel.ouroboros.info
  - conda-forge
```

After installation of conda add the following line to your **.bashrc**
/ **.zshrc** to be able to use the command **conda** without using the full
path **/opt/conda/bin/conda**:
```bash
. /opt/conda/etc/profile.d/conda.sh
```

Restart your terminal now.

Then install **conda-build** and **conda-verify**:
```bash
conda install conda-build
conda install conda-verify
```

**Note:**
>    If you want to use the **conda-env-manager** (**cenv**), install it with
>    ``conda create -n cenv_tool cenv``
>
>    You could run **cenv** now using ``/shared/conda/envs/cenv_tool/bin/cenv``
>    but this is a little bit too much writing, so you should create an alias.
>
>    As an example how to create such an alias for **bash** or **zsh** you could
>    add the following line to your **.bashrc** / **.zshrc**:
>    ```bash
>    alias cenv=/shared/conda/envs/cenv_tool/bin/cenv
>    ```
>    For more details about how to use **cenv** and configure it see
>    [cenv](## conda-env-manager: cenv).

Now you have installed all relevant parts of conda and can use the commands
``conda``, ``conda build`` and ``cenv``.


## cheatsheets

### conda

task | conda-command
---- | ---------------
install pkg into current env     | `conda install <pkg>`
install pkg into specific env    | `conda install -n <env_name> <pkg>`
install pkg from url             | `conda install --channel <url> <pkg>`
update a pkg                     | `conda update -n <env_name> <pkg>`
update conda                     | `conda update conda`
uninstall a pkg                  | `conda remove -n <env_name> <pkg>`
create an env                    | `conda create -n <env_name>`
activate an env                  | `conda activate <env_name>`
deactivate current env           | `conda deactivate`
search available pkgs            | `conda search <to_search>`
list pkgs of current env         | `conda list`
list pkgs of specific env        | `conda list -n <env_name>`
create environment.yml file      | `conda env export > environment.yml`
list all envs                    | `conda env list`
show info about conda            | `conda info`


### conda-env-manager: cenv
For details about **cenv** see [cenv](## conda-env-manager: cenv).

**Note:**
>    All cenv-commands have to be executed inside the project-folder.

task | cenv-command
---- | ------------
install pkg into current env   | modify projects **meta.yaml**, run `cenv -a`
install pkg into specific env  | modify projects **meta.yaml**, run `cenv -a`
install pkg from url           | modify projects **meta.yaml**, run `cenv -a`
update a pkg                   | modify projects **meta.yaml**, run `cenv -a`
update conda                   | `conda update conda`
uninstall a pkg                | modify projects **meta.yaml**, run `cenv -a`
create an env                  | modify projects **meta.yaml**, run `cenv -a`
activate an env                | `conda activate <env_name>`
deactivate current env         | `conda deactivate`
search available pkgs          | `cenv -s <pkg>`
list pkgs of current env       | modify projects **meta.yaml**, run `cenv -i`
list pkgs of specific env      | modify projects **meta.yaml**, run `cenv -i`
create environment.yml file    | modify projects **meta.yaml**, run `cenv -a`
list all envs                  | modify projects **meta.yaml**, run `cenv -i`
show info about conda          | `conda info`


## conda environments
* Development inside isolated environments
* Conda-environments can be
    * created and remove
    * activated and deactivated
    * exported (to yaml-files) and imported (from yaml-files)
* includes the exactly definition of the used packages and their versions
* are part of the package. So they should be stored inside the same
  git-repository

=> portable and explicit

**Additional information:**
>    See [conda environments description](https://conda.io/projects/conda/en/latest/user-guide/concepts.html#conda-environments)
>    and [conda environments](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)
>    for the official documentation.

**Definition:**
>    A conda environment is a directory that contains a specific collection of
>    conda packages that you have installed. For example, you may have one
>    environment with NumPy 1.7 and its dependencies, and another environment
>    with NumPy 1.6 for legacy testing. If you change one environment, your other
>    environments are not affected. You can easily activate or deactivate
>    environments, which is how you switch between them. You can also share your
>    environment with someone by giving them a copy of your environment.yaml
>    file.
>
>    (as defined at https://docs.conda.io)



### creation of conda environments
**Note:**
>    This part is only relevant if you do not use **cenv**.
>    If you use **cenv** the only important environment-commands are:
>
>    * `conda activate <env_name>`: Activate a conda-environment
>    * `conda deactivate`: Deactivate current conda-environment

To create new conda environment run:
```bash
conda create -n <environment_name> \
    python=<python_version> \
    <dependency_1> \
    <dependency_2> ...
```

The version of each dependency can be specified with
`conda create -n example python=3.6 pandas=0.24.0`.

This creates the conda environment **example** with python in version 3.6 and
pandas in version 0.24.0.
Additionally all dependencies of the defined packages are also installed
into the conda environment like **numpy** for example as a dependency of
**pandas**.

Conda environments can also be created from an environment-definition-file,
for example the **environment.yml**.
To create an environment from such a file run
`conda env create -f environment.yml`.

### activation of conda environments
After we created the conda-environment, we can use it.
To activate this environment run `conda activate <environment_name>`.

In case of the example environment we created we can activate it using
`conda activate example`.

The current conda environment can be deactivated using `conda deactivate`.


To ensure to use the correct python-interpreter check it using `which python`.

If the conda environment is activated correctly the output contains
something like **/shared/conda/envs/example/bin/python**.

If no environment is active this should be **/usr/bin/python*.


### usage of conda environments in your IDE
 * VS Code: https://code.visualstudio.com/docs/python/environments#_manually-specify-an-interpreter
 * WingIDE: https://docs.anaconda.com/anaconda/user-guide/tasks/integration/wing/
 * PyCharm: https://www.jetbrains.com/help/pycharm/conda-support-creating-conda-virtual-environment.html


### list existing conda environments
To list all conda environments currently available on your machine, run
`conda env list`.
The active conda-environment will be marked (in case of no environment is
currently active this will be the base/root environment).

### modification of conda environments
To delete an existing conda-environment run
`conda remove -n <environment_name> --all`.

To add additional dependencies to a conda-environment, activate it and install
the dependency:
```bash
conda activate <environment_name>
conda install <dependency[=version]>
```

This also can be done using
`conda install -n <environment_name> <dependency[=version]>`.

To remove a dependency from a conda-environment run:
```bash
conda activate <environment_name>
conda remove <dependency>
```

This can also be done using
`conda remove -n <environment_name> <dependency[=version]>`.

To check if a conda-package is available from the channels and in which
versions, you can search for this package using:
`conda search <package_name>`.

**Example:**
>    `conda search pandas`
>
>    This results in the following output:
>
>    ```bash
>    Loading channels: ...working... done
>    # Name    Version                Build    Channel
>    pandas      0.8.1           np16py26_0    pkgs/free
>    pandas      0.8.1           np16py27_0    pkgs/free
>    pandas      0.8.1           np17py26_0    pkgs/free
>    pandas      0.8.1           np17py27_0    pkgs/free
>    pandas      0.9.0           np16py26_0    pkgs/free
>    pandas      0.9.0           np16py27_0    pkgs/free
>    pandas      0.9.0           np17py26_0    pkgs/free
>    ...
>    pandas     0.23.4       py27h04863e7_0    pkgs/main
>    pandas     0.23.4    py27h637b7d7_1000    conda-forge
>    pandas     0.23.4       py27hf8a1672_0    conda-forge
>    pandas     0.23.4       py35h04863e7_0    pkgs/main
>    pandas     0.23.4       py35hf8a1672_0    conda-forge
>    pandas     0.23.4       py36h04863e7_0    pkgs/main
>    pandas     0.23.4    py36h637b7d7_1000    conda-forge
>    pandas     0.23.4       py36hf8a1672_0    conda-forge
>    pandas     0.23.4       py37h04863e7_0    pkgs/main
>    pandas     0.23.4    py37h637b7d7_1000    conda-forge
>    pandas     0.23.4       py37hf8a1672_0    conda-forge
>    pandas     0.24.0       py27he6710b0_0    pkgs/main
>    pandas     0.24.0       py27hf484d3e_0    conda-forge
>    pandas     0.24.0       py36he6710b0_0    pkgs/main
>    pandas     0.24.0       py36hf484d3e_0    conda-forge
>    pandas     0.24.0       py37he6710b0_0    pkgs/main
>    pandas     0.24.0       py37hf484d3e_0    conda-forge
>    ```


### exporting conda environments
To export the current state of a conda-environment to an ``environment.yml``
file so it can be used by other developers run
`conda env export > environment.yml`.


### example-workflow
**Note:**
>    This workflow demonstrates the workflow for conda environments without
>    using **cenv**.

#### 1. check current path for the python-interpreter
Show the path of the current python-interpreter: `which python`.
This should be **/usr/bin/python**.

#### 2. list all available conda environments
Now we want to see all available conda-environments using `conda env list`.

If we did not create any conda-environment this will look like:
```bash
# conda environments:
#
base                  *  /opt/conda
```

#### 3. create new conda environment
We create a new conda-environment named ``testenv`` with ``python`` and
``attrs`` installed inside: `conda create -n testenv python=3.6 attrs`.

The output will look like:
```bash
Collecting package metadata: ...working... done
Solving environment: ...working... done
initializing UnlinkLinkTransaction with
  target_prefix: /shared/conda/envs/testenv
  unlink_precs:

  link_precs:
    defaults::ca-certificates-2019.1.23-0
    defaults::libgcc-ng-8.2.0-hdf63c60_1
    defaults::libstdcxx-ng-8.2.0-hdf63c60_1
    defaults::libffi-3.2.1-hd88cf55_4
    defaults::ncurses-6.1-he6710b0_1
    defaults::openssl-1.1.1a-h7b6447c_0
    defaults::xz-5.2.4-h14c3975_4
    defaults::zlib-1.2.11-h7b6447c_3
    defaults::libedit-3.1.20181209-hc058e9b_0
    defaults::readline-7.0-h7b6447c_5
    defaults::tk-8.6.8-hbc83047_0
    defaults::sqlite-3.26.0-h7b6447c_0
    defaults::python-3.6.8-h0371630_0
    defaults::attrs-18.2.0-py36h28b3542_0
    defaults::certifi-2018.11.29-py36_0
    defaults::setuptools-40.7.3-py36_0
    defaults::wheel-0.32.3-py36_0
    defaults::pip-19.0.1-py36_0

## Package Plan ##
  environment location: /shared/conda/envs/testenv

  added / updated specs:
    - attrs
    - python=3.6

The following NEW packages will be INSTALLED:

  attrs              pkgs/main/linux-64::attrs-18.2.0-py36h28b3542_0
  ca-certificates    pkgs/main/linux-64::ca-certificates-2019.1.23-0
  certifi            pkgs/main/linux-64::certifi-2018.11.29-py36_0
  libedit            pkgs/main/linux-64::libedit-3.1.20181209-hc058e9b_0
  libffi             pkgs/main/linux-64::libffi-3.2.1-hd88cf55_4
  libgcc-ng          pkgs/main/linux-64::libgcc-ng-8.2.0-hdf63c60_1
  libstdcxx-ng       pkgs/main/linux-64::libstdcxx-ng-8.2.0-hdf63c60_1
  ncurses            pkgs/main/linux-64::ncurses-6.1-he6710b0_1
  openssl            pkgs/main/linux-64::openssl-1.1.1a-h7b6447c_0
  pip                pkgs/main/linux-64::pip-19.0.1-py36_0
  python             pkgs/main/linux-64::python-3.6.8-h0371630_0
  readline           pkgs/main/linux-64::readline-7.0-h7b6447c_5
  setuptools         pkgs/main/linux-64::setuptools-40.7.3-py36_0
  sqlite             pkgs/main/linux-64::sqlite-3.26.0-h7b6447c_0
  tk                 pkgs/main/linux-64::tk-8.6.8-hbc83047_0
  wheel              pkgs/main/linux-64::wheel-0.32.3-py36_0
  xz                 pkgs/main/linux-64::xz-5.2.4-h14c3975_4
  zlib               pkgs/main/linux-64::zlib-1.2.11-h7b6447c_3

Proceed ([y]/n)? y

Preparing transaction: ...working... done
Verifying transaction: ...working... done
Executing transaction: ...working... ===> LINKING PACKAGE: defaults::ca-certificates-2019.1.23-0 <===
  prefix=/shared/conda/envs/testenv
  source=/shared/conda/pkgs/ca-certificates-2019.1.23-0

...

===> LINKING PACKAGE: defaults::python-3.6.8-h0371630_0 <===
  prefix=/shared/conda/envs/testenv
  source=/shared/conda/pkgs/python-3.6.8-h0371630_0

===> LINKING PACKAGE: defaults::attrs-18.2.0-py36h28b3542_0 <===
  prefix=/shared/conda/envs/testenv
  source=/shared/conda/pkgs/attrs-18.2.0-py36h28b3542_0

...

===> LINKING PACKAGE: defaults::pip-19.0.1-py36_0 <===
  prefix=/shared/conda/envs/testenv
  source=/shared/conda/pkgs/pip-19.0.1-py36_0

done
#
# To activate this environment, use
#
#     $ conda activate testenv
#
# To deactivate an active environment, use
#
#     $ conda deactivate
```

#### 4. check for available conda environments, again
Again we list all available conda environments: `conda env list`.

This results in:
```bash
# conda environments:
#
base                  *  /opt/conda
testenv                  /shared/conda/envs/testenv
```
This time the conda-environment ``testenv`` is listed, too.

#### 5. activate the created conda environment
To use the conda-environment ``testenv``, we activate it:
`conda activate testenv`.

Your prompt in current shell will look like the following now:
`(testenv) exampleuser@examplehost:~$`.

#### 6. check the changed path for the python-interpreter
To ensure we really use this conda-environment now, we want to know which
python-interpreter is used. When we run `which python` again, this will give us
something like **/shared/conda/envs/testenv/bin/python**.
This means we sucessfully activated the conda-environment **testenv**.

#### 7. show installed packages of the conda environment
We want to know which packages with which versions are installed in our
conda-environment, so we list all these packages with: `conda list`.

This results in:
```bash
# packages in environment at /shared/conda/envs/testenv:
#
# Name                    Version                   Build  Channel
attrs                     18.2.0           py36h28b3542_0    defaults
ca-certificates           2019.1.23                     0    defaults
certifi                   2018.11.29               py36_0    defaults
libedit                   3.1.20181209         hc058e9b_0    defaults
libffi                    3.2.1                hd88cf55_4    defaults
libgcc-ng                 8.2.0                hdf63c60_1    defaults
libstdcxx-ng              8.2.0                hdf63c60_1    defaults
ncurses                   6.1                  he6710b0_1    defaults
openssl                   1.1.1a               h7b6447c_0    defaults
pip                       19.0.1                   py36_0    defaults
python                    3.6.8                h0371630_0    defaults
readline                  7.0                  h7b6447c_5    defaults
setuptools                40.7.3                   py36_0    defaults
sqlite                    3.26.0               h7b6447c_0    defaults
tk                        8.6.8                hbc83047_0    defaults
wheel                     0.32.3                   py36_0    defaults
xz                        5.2.4                h14c3975_4    defaults
zlib                      1.2.11               h7b6447c_3    defaults
```
So we use python 3.6.8 and attrs is installed, too (in version 18.2.0).


#### 8. search for another conda-package
Assumed we would require another python package named marshmallow now, we
want to know if this package is available in current conda-channels.
So we run: `conda search marshmallow`.

The list of available versions will look like:
```bash
Loading channels: ...working... done
# Name                       Version           Build  Channel
marshmallow                   2.10.0          py27_0  conda-forge
marshmallow                   2.10.0          py34_0  conda-forge
marshmallow                   2.10.0          py35_0  conda-forge
...
marshmallow                   2.18.0          py36_0  pkgs/main
marshmallow                   2.18.0          py37_0  pkgs/main
marshmallow                  3.0.0b2          py36_0  None
marshmallow                  3.0.0b7            py_0  conda-forge
marshmallow                  3.0.0b8          py27_0  pkgs/main
marshmallow                  3.0.0b8          py35_0  pkgs/main
marshmallow                  3.0.0b8          py36_0  pkgs/main
marshmallow                  3.0.0b8          py37_0  pkgs/main
marshmallow                  3.0.0b8            py_0  conda-forge
marshmallow                 3.0.0b19            py_0  conda-forge
marshmallow                 3.0.0rc1            py_0  conda-forge
```

#### 9. install additional conda-package to the conda environment
We want to add ``marshmallow`` in version ``3.0.0rc1`` to be installed in our
environment.
To install it we run: `conda install marshmallow=3.0.0rc1`.

The process will look like:
```bash
Collecting package metadata: ...working... done
Solving environment: ...working... done
initializing UnlinkLinkTransaction with
  target_prefix: /shared/conda/envs/testenv
  unlink_precs:

  link_precs:
    conda-forge::marshmallow-3.0.0rc1-py_0

## Package Plan ##

  environment location: /shared/conda/envs/testenv

  added / updated specs:
    - marshmallow=3.0.0rc1

The following NEW packages will be INSTALLED:

  marshmallow        conda-forge/noarch::marshmallow-3.0.0rc1-py_0

Proceed ([y]/n)? y

Preparing transaction: ...working... done
Verifying transaction: ...working... done
Executing transaction: ...working... ===> LINKING PACKAGE: conda-forge::marshmallow-3.0.0rc1-py_0 <===
  prefix=/shared/conda/envs/testenv
  source=/shared/conda/pkgs/marshmallow-3.0.0rc1-py_0

done
```
If we run ``conda env list`` again, marshmallow will be listed now.


#### 10. export the environment-definition
To allow others to use this conda-environment, we want to export it into a
definition-file.
This is done using `conda env export > environment.yml`.

The content of the file will be like:
```bash
name: testenv
channels:
  - defaults
  - local
  - https://www.repository-channel.ouroboros.info
  - conda-forge
dependencies:
  - attrs=18.2.0=py36h28b3542_0
  - ca-certificates=2019.1.23=0
  - certifi=2018.11.29=py36_0
  - libedit=3.1.20181209=hc058e9b_0
  - libffi=3.2.1=hd88cf55_4
  - libgcc-ng=8.2.0=hdf63c60_1
  - libstdcxx-ng=8.2.0=hdf63c60_1
  - marshmallow=3.0.0rc1=py_0
  - ncurses=6.1=he6710b0_1
  - openssl=1.1.1a=h7b6447c_0
  - pip=19.0.1=py36_0
  - python=3.6.8=h0371630_0
  - readline=7.0=h7b6447c_5
  - setuptools=40.7.3=py36_0
  - sqlite=3.26.0=h7b6447c_0
  - tk=8.6.8=hbc83047_0
  - wheel=0.32.3=py36_0
  - xz=5.2.4=h14c3975_4
  - zlib=1.2.11=h7b6447c_3
prefix: /shared/conda/envs/testenv
```
This defines the name of the conda-environment (``name``), which channels
where available when creating the environment (``channels``), where the
environment is installed (``prefix``) and which dependencies in which
versions have to be installed inside (``dependencies``).


#### 11. deactivate and remove the conda environment
If we would like to remove the conda environment **testenv** we first have to
deactivate it using `conda deactivate`.

We ensure the environment to be deactivated checking the python-interpreter
path with `which python`.

Now we remove the environment with `conda remove -n testenv --all`.

If we list the available conda-environment with ``conda env list``, the
**testenv** will not be listed anymore.


#### 12. recreate the conda environment from environment-definition-file
To recreate the conda-environment from the definiton-file ``environment.yml``
we created, we run `conda env create -f environment.yml`.

To check if the environment was created we can use ``conda env list``, again.


## .condarc
The **.condarc** is the configuration file of conda.
Inside this file the available channels, the path of the conda environments,
etc. are defined.
Following the content of this file as we use it:
```yaml
allow_other_channels: True
show_channel_urls: True

pkgs_dirs:
  - /shared/conda/pkgs

envs_dirs:
  - /shared/conda/envs

channels:
  - defaults
  - local
  - https://www.repository-channel.ouroboros.info
  - conda-forge
```


**Note:**
>    As we installed conda at **/opt/conda**, the **.condarc** should be
>    created at **/opt/conda/.condarc**.


**Tip:**
>    See
>    [.condarc](https://conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html)
>    for the official documentation of the **.condarc**.

## channels
Conda channels serve conda-packages.
As default the following conda-channels are recommended:
* Anaconda-channel: default
* community-channel: conda-forge
* my-channel: https://repository-channel.ouroboros.info/


### availability of packages
* Normally all required packages should be available in the defined channels
* if they do not exist yet, they must be built yourself


## conda-packages
**Tip:**
>    See
>    [conda packages description](https://conda.io/projects/conda/en/latest/user-guide/concepts.html#conda-packages)
>    and
>    [conda packages](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-pkgs.html#)
>    for the official documentation for conda-packages.

**Definition:**
>    A conda package is a compressed tarball file that contains system-level
>    libraries, Python or other modules, executable programs and other
>    components. Conda keeps track of the dependencies between packages and
>    platforms.
>
>    Conda packages are downloaded from remote channels, which are URLs to
>    directories containing conda packages.
>    [...]
>    A conda package includes a link to a tarball or bzipped tar archive, with
>    the extension “.tar.bz2”, which contains metadata under the info/ directory
>    and a collection of files that are installed directly into an install prefix.
>
>    During the install process, files are extracted into the install prefix,
>    except for files in the info/ directory. Installing the files of a conda
>    package into an environment can be thought of as changing the directory to
>    an environment, and then downloading and extracting the .zip file and its
>    dependencies—all with the single conda install [packagename] command.
>
>    (as defined at https://docs.conda.io)


### convert pypi-packages to conda-packages
This requires **conda-build** to be installed into the root- / base-conda.

**Note:**
>    **Installing ``conda-build``**
>
>    ```bash
>    conda update conda
>    conda install conda-build
>    ```

The conversion from a pip- to conda-package is relatively simple.
Use `conda skeleton pypi <package> --python=<python_version>` to create the
skeleton used for building the package.

This creates a subfolder in current path with the name of the package.
This folder contains the **meta.yaml** with the building-information to
create the conda-package.
To build the package use
`conda build <path_to_package_folder> --python=<python_version>`.

**Tip:**
>    For details about how to use the `conda skeleton`-command see
>    [conda skeleton pypi](https://docs.conda.io/projects/conda-build/en/latest/source/commands/conda-skeleton-pypi.html).
>
>    For details about building conda-packages in general see
>    [conda build](https://docs.conda.io/projects/conda-build/en/latest/index.html).


#### possible errors during build
Two possible errors are possible during the build-process:

1. A dependency of the conda-package is not available as a conda-package yet
2. The **meta.yaml** is missing a dependency required by the package


##### solving error of type 1
The required package must be built, too.
To create it run the same commands as before for this package.
It is possible, the building-process of this package results in a dependency
error, too, etc.
This dependency chain has to be solved creating each package.


##### solving error of type 2
Read the error-message carefully.
It is possible the name of the required package is listed.
Then add the name of this package to the **meta.yaml** at
**requirements/run**.
If the name of the required package is not listed inside the error-message
you'll have to search for the package and its dependencies online and then add
the found requirements to the **meta.yaml** file.


### create selfbuild conda-packages
This requires **conda-build** to be installed into the root- / base-conda.

**Note:**
>    **Installing conda-build**
>
>    ```bash
>    conda update conda
>    conda install conda-build
>    ```

**Tip:**
>    See
>    [conda build](https://docs.conda.io/projects/conda-build/en/latest/index.html)
>    and
>    [meta.yaml](https://docs.conda.io/projects/conda-build/en/latest/source/define-metadata.html)
>    for detailed documentation.


#### projektstructure
The project must have the following minimal structure:
```bash
    <project_name>
    ├── conda-build
    │   └── meta.yaml
    ├── __init__.py
    ├── setup.py
    └── <project_name>
        ├── __init__.py
        ├── <example_code_file>
        └── tests
```

##### setup.py
The **setup.py** has following content:
```python
# -*- coding: utf-8 -*-


import subprocess

from setuptools import find_packages
from setuptools import setup


setup(name="<project_name>",
      version=(subprocess.check_output(['git', 'describe', '--tag'])
               .strip()
               .decode('ascii')
               .replace('-', '_')),
      packages=find_packages(),
      zip_safe=False)
```
In your project you must replace **<project_name>** with the correct name of
your project.


##### meta.yaml
The **meta.yaml** at **<project_name>/conda-build/meta.yaml** has following
content:
```yaml
{% set data = load_setup_py_data() %}

package:
    name: "<package_name>"
    version: {{ data.get("version") }}

source:
    path: ..

build:
    build: {{ environ.get('GIT_DESCRIBE_NUMBER', 0) }}
    preserve_egg_dir: True
    script: python -m pip install --no-deps --ignore-installed .
    #entry_points:
    #  - your_command = your_module:main

requirements:
    build:
      - python 3.6.7
      - pip
      - setuptools
    run:
      - python 3.6.7

test:
    imports:
        - <package_name>
        - <package_name>.<example_code_file>
    commands:
        - pytest <package_name>/tests
    requires:
        - pytest
        - pytest-lazy-fixture
        - pytest-xdist
    source_files:
        - <package_name>/tests

extra:
    env_name: <project_name>
```


**Note:**
>    In your project you have to replace **<package_name>** and
>    **<example_code_file>** with your names.
>    You must also define the dependencies of your project at
>    **requirements/run**.
>    For example, to add **marshmallow** as dependency the requirements-part
>    would look like:
>
>    ```yaml
>    requirements:
>        build:
>          - python 3.6.7
>          - setuptools
>        run:
>          - python 3.6.7
>          - marshmallow >=3.0.0*
>    ```
>
>    You also have to change the name of the conda-environment in the
>    **extra-section** if you use **cenv**.


**Tip:**
>    Additional documentation and information about the dependency-definition
>    inside the **meta.yaml** can be found at:
>
>    * https://docs.conda.io/projects/conda-build/en/latest/source/define-metadata.html#run
>    * https://docs.conda.io/projects/conda-build/en/latest/source/package-spec.html#build-version-spec


#### building the conda-package
**Attention:**
>    The version of the package to build is extracted from the projects
>    **git-tag**.
>    So you first have to commit your changes and tag the current state of the
>    project in git with the version number to use.
>    Only after this you can create the conda-package.


With these two files (**meta.yaml** and **setup.py**) and the project
structured like explained before, the conda-package for this project can be
built with
`conda build <path_to_project_folder> --python=<python_version>`.

Now the package is available as a conda-package from the local conda-build
folder.

The created conda-package is stored as a **.tar.bz2**-file at
**/opt/conda/conda-bld/linux-64/<package_name>.tar.bz2**.

You can check the conda-package to be available from the local-channel (your
**conda-build**-folder) with `conda search <package_name> --use-local`.


##### add package to the conda-repository-channel
To be available for others you have to add the created conda-package to
your own conda-repository-channel.

Copy the package to the channel with:
`scp /opt/conda/conda-bld/linux-64/<package_name>.tar.bz2 <user>@<host>:<conda_folder>/conda-bld/linux-64/.`.

Note:
>    Before you have to change the **<package_name>** to your package name and
>    the correct **<user>**, **<host>** and **<conda_folder>**.

Now you have to update the index of the conda-repository on the channel.
Connect to the repository-server with ``ssh``.
Now run `<conda_folder>/bin/conda index <conda_folder>/conda/conda-bld`.

The package is available from the conda-repository-channel now.
You can check the conda-package to be available with
`conda search <package_name>`.

In the resulting list there should be the package and as channel the
conda-repository-channel.


## conda-env-manager: cenv
Due to the redundant dependency informations inside the **meta.yaml** (required
to create the conda-package) and the **environment.yml** (as definition file
for the conda-environment during development and for production), **cenv**
(short form for **conda-env-manager**) was created to make the **meta.yaml**
the only relevant file and to create and update conda-environment from the
definition inside this **meta.yaml**.
The name of the conda-environment to create / update is defined in the section
**extra** and the variable **env_name** inside the **meta.yaml**.

**cenv** can be run in **default-mode** or **auto-mode**.
Run in **default-mode** it displays all relevant commands for your project to
run to create or update your project.
This includes

* creation of a backup if the environment already exists followed by the
  removal of the previous environment.
* creation of the environment as defined in the **meta.yaml**.
  If any failures occured during creation and the backup was created, the
  command to reset the backup-version can be used.
* finally the commands to activate and export the environment-definition to an
  **environment.yml** to be copied to systems not using **cenv** are shown.

In **auto-mode** these steps are run automatically by **cenv**.
If it is required to remove an existing conda environment, before recreation,
the user is asked for confirmation before the process starts (this can be
skipped using `cenv -ay`).

**Attention:**
>    Each environment is only created, updated and modified using ``cenv``!
>    This means the commands ``conda install``, ``conda remove`` are not used
>    anymore.
>    Changes of the dependencies of the environment are defined inside the
>    **meta.yaml** and are applied by using `cenv`.
>
>    This means:
>
>    * new dependency required => add it in **meta.yaml** and run ``cenv``.
>    * dependency not needed anymore => remove it from **meta.yaml** and run
>      ``cenv``.
>    * need of another version of dependency => change the version of dependency
>      in **meta.yaml** and run ``cenv``.


This reduces the conda commands to use to the following:
* `conda activate ...`
* `conda deactivate`
* `conda info`
* `cenv`


### installation
**cenv** itself is a conda-package so it can easily be installed using conda.
To install it run `conda create -n cenv_tool cenv`.

This creates a new conda-environment names **cenv_tool** with only **cenv**
as a major-dependency.

You could run **cenv** now using ``/shared/conda/envs/cenv_tool/bin/cenv``
but this is a little bit too much writing, so you should create an alias.

As an example how to create such an alias for **bash** or **zsh** you could
add the following line to your **.bashrc** / **.zshrc**:
```bash
alias cenv=/shared/conda/envs/cenv_tool/bin/cenv
```

Now **cenv** can be run only using `cenv`.

**Attention:**
>    **cenv** uses the path **/opt/conda** as default conda-installation-folder
>    and **/shared/conda/envs** as default conda-environments-folder.
>    You can overwrite these settings with a **.cenv.yml** inside your
>    home-folder with the following content:
>
>    ```yaml
>    conda_folder: /opt/conda
>    env_folder: /shared/conda/envs
>    ```
>
>    There you can define your own conda-installation-path and the
>    conda-environments-folder.


### meta.yaml
Each project using **cenv** requires the project-configuration in the
**extra**-section of the **meta.yaml**.
There you can define the name of the projects conda environment at
**env_name**.

All other parts of the **meta.yaml** have to be as defined before in this
documentation.


#### example for a valid meta.yaml used by cenv
```yaml
{% set data = load_setup_py_data() %}

package:
    name: "example_package"
    version: {{ data.get("version") }}

source:
    path: ..

build:
    build: {{ environ.get('GIT_DESCRIBE_NUMBER', 0) }}
    preserve_egg_dir: True
    script: python -m pip install --no-deps --ignore-installed .

requirements:
    build:
      - python 3.6.8
      - pip
      - setuptools
    run:
      - python 3.6.8
      - attrs >=18.2
      - jinja2 >=2.10
      - ruamel.yaml >=0.15.23
      - six >=1.12.0
      - yaml >=0.1.7
      - marshmallow >=3.0.0rc1*

test:
    imports:
        - example_package

extra:
    env_name: example
```

**Attention:**
>    In the **requirements-run**-section the version of each package has to be
>    defined!
>    Not defining a version will not create or update a conda environment,
>    because this is not the purpose of the conda-usage.
>    The validity of the **meta.yaml** is checked in **cenv** using the
>    **marshmallow** package.


### usage
**cenv** can be run in default-mode or in auto-mode.
In default-mode **cenv** will only display the commands required to create
/ update the conda-environment of the current project.
No modifications are made by **cenv** itself.

In auto-mode all commands are executed by **cenv**.
If a removal of the existing environment is required to update the environment
the user is asked for confirmation.
The auto-mode is triggered by the flag `-a`.

`cenv` can be run with the following flags:

* `-a`: triggers the auto-mode
* `-i`: show informations about current project and conda itself
* `-d`: show manual of cenv using vmd-tool and exit
* `-v`: show version of cenv and exit
* `-y`: don't ask for cofirmation in auto-mode
* `-s <package_name>`: Search for availability of passed package in
  conda-channels and pip (if not available in conda-channels).
  If only found on pypi, the commands to convert this package to a
  conda-package are shown. It can be used as a replacement for the ``conda
  search``-command.

Attention:
>    ``cenv`` can only update the environment if it is not activated.
>    So ensure the environment to be deactivated before running ``cenv``.


#### cenv in default-mode
Change to the path of your project.
The **meta.yaml** has to contain the additional **extra**-section to defined
the name of the conda-environment like the following:
```yaml
extra:
    env_name: example
```

Now you can run **cenv** with `cenv`.

Example output for ``cenv`` without any argument:
```bash
┃ Commands to update cenv_dev
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Note: You can also run:
    cenv -a
to run the following commands in auto-mode.

ATTENTION:
The required env already exists. Remove it before recreation.

Create a backup using:
    conda create -n cenv_dev_backup --clone cenv_dev

Remove env with:
    conda remove -n cenv_dev --all

Create env with:
    conda create -n cenv_dev "python=3.6.8" \
        "attrs>=18.2" \
        "jinja2>=2.10" \
        "ruamel.yaml>=0.15.23" \
        "six>=1.12.0" \
        "yaml>=0.1.7" \
        "marshmallow>=3.0.0rc1*" \
        "vmd>=0.1.1"

If any failure occured during creation reset from backup:
    conda create -n cenv_dev --clone cenv_dev_backup

Now remove the backup:
    conda remove -n cenv_dev_backup --all

Activate env using:
    conda activate cenv_dev

Export env to environment.yml:
    conda env export -n cenv_dev > environment.yml
```

Example output for ``cenv -i``:
```bash
┃ Available environments
┗━━━━━━━━━━━━━━━━━━━━━━━━
    - new_env
    - cenv_tool
    - cenv
    - tools_py2_env
    - tools_py3_env
    - example
    - grip
    - cenv_dev

┃ Project info
┗━━━━━━━━━━━━━━
                 name ┃  cenv_dev
                  git ┃  True
           env exists ┃  True

┃ Dependencies
┗━━━━━━━━━━━━━━
                       python  3.6.8
                        attrs  >=18.2
                       jinja2  >=2.10
                  ruamel.yaml  >=0.15.23
                          six  >=1.12.0
                         yaml  >=0.1.7
                  marshmallow  >=3.0.0rc1*
                          vmd  >=0.1.1

┃ Packages installed in cenv_dev
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
# packages in environment at /shared/conda/envs/cenv_dev:
#
# Name                    Version                   Build  Channel
attrs                     18.2.0           py36h28b3542_0    defaults
ca-certificates           2019.1.23                     0    defaults
certifi                   2018.11.29               py36_0    defaults
jinja2                    2.10                     py36_0    defaults
libedit                   3.1.20181209         hc058e9b_0    defaults
libffi                    3.2.1                hd88cf55_4    defaults
libgcc-ng                 8.2.0                hdf63c60_1    defaults
libstdcxx-ng              8.2.0                hdf63c60_1    defaults
markdown                  2.6.8                    py36_0    defaults
markupsafe                1.1.0            py36h7b6447c_0    defaults
marshmallow               3.0.0rc1                   py_0    conda-forge
ncurses                   6.1                  he6710b0_1    defaults
openssl                   1.1.1a               h7b6447c_0    defaults
pip                       19.0.1                   py36_0    defaults
python                    3.6.8                h0371630_0    defaults
readline                  7.0                  h7b6447c_5    defaults
ruamel.yaml               0.15.23                  py36_0    https://www.repository-channel.ouroboros.info
setuptools                40.7.3                   py36_0    defaults
six                       1.12.0                   py36_0    defaults
sqlite                    3.26.0               h7b6447c_0    defaults
tk                        8.6.8                hbc83047_0    defaults
vmd                       0.1.1                    py36_0    local
wheel                     0.32.3                   py36_0    defaults
xz                        5.2.4                h14c3975_4    defaults
yaml                      0.1.7                had09818_2    defaults
zlib                      1.2.11               h7b6447c_3    defaults

┃ Commands to update cenv_dev
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Note: You can also run:
    cenv -a
to run the following commands in auto-mode.

ATTENTION:
The required env already exists. Remove it before recreation.

Create a backup using:
    conda create -n cenv_dev_backup --clone cenv_dev

Remove env with:
    conda remove -n cenv_dev --all

Create env with:
    conda create -n cenv_dev "python=3.6.8" \
        "attrs>=18.2" \
        "jinja2>=2.10" \
        "ruamel.yaml>=0.15.23" \
        "six>=1.12.0" \
        "yaml>=0.1.7" \
        "marshmallow>=3.0.0rc1*" \
        "vmd>=0.1.1"

If any failure occured during creation reset from backup:
    conda create -n cenv_dev --clone cenv_dev_backup

Now remove the backup:
    conda remove -n cenv_dev_backup --all

Activate env using:
    conda activate cenv_dev

Export env to environment.yml:
    conda env export -n cenv_dev > environment.yml
```

#### cenv in auto-mode
Usage is the same as in default-mode.
You can also add the flag `-i` to show details before the auto-creation
/ auto-update.
The difference between **default-mode** and **auto-mode** is that all commands
required to create or update the projects conda-environment are run
automatically.
This **auto-mode** is started when using the `-a` flag.
If a removal of an existing environment is necessary, the user is asked for
confirmation.
The creation of the backup of the previous environment ensures to undo changes
if any error occures during recreation of the environment.
Run **cenv** in auto-mode using `cenv -a`.

Example for the output of this command:
```bash
Updating env [AUTOMODE]

┃ ATTENTION: this will remove the existing env before recreation.
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Continue? [y/n]: y
```

The user is asked for confirmation to continue because the existing
conda-environment has to be removed (after creating a backup) to recreate the
conda-environment from the information inside the **meta.yaml**.

After confirmed the output will look like:
```bash
┣━━ Cloning existing env as backup ...
┣━━ Removing existing env ...
┣━━ Creating env ...
┣━━ Removing backup ...
┗━━ Exporting env to environment.yml ...
```

#### showing more detailed help for cenv
**cenv** can be run with the `-h` flag to show help for the
``cenv``-command. It also supports the `-d` flag to show a more detailed
documentation.

Example output using `cenv -d`:
```bash
1 cenv

  1.1 Installation

      Create new conda-environment with this package as only requirement.

      1 ┃ conda create -n cenv_tool cenv



      Then create an alias in your .zshrc / .bashrc like the following

      1 ┃ alias cenv="/shared/conda/envs/cenv_tool/bin/cenv"



      Now you can use cenv from terminal with only typing cenv.
    For example:

      1 ┃ cenv --help



      cenv can be run in default-mode or in auto-mode.
    In default-mode cenv will only display the commands required to create
    / update the conda-environment of the current project.
    No modifications are made by cenv itself.

      In auto-mode all commands are executed by cenv. If removing of the
    existing environment is required to update the environment the user is asked
    for confirmation.
    The auto-mode is triggered by the flag -a.

      cenv can be run with the following flags:
     -a: triggers the auto-mode
     -i: show informations about current project and conda itself
     -d: show this README with vmd and exit
     -v: show cenv-version and exit
    * --search <package_name>: Searchs for avaiability of package in
      conda-channels. If package could not be found in conda-channels, searches on
      pypi and shows the commands to convert the package from a pypi-package to
      a conda-package. This can be used as a replacement for the conda
      search-command.

    ┃ "ATTENTION:cenv can only update the environment if it is not activated.
    ┃ So ensure the environment to be deactivated before running cenv."

      cenv requires a meta.yaml file in projects subfolder
    conda-build/meta.yaml.

      In the extra-section the name of the conda-environment to create / update
    is defined.

      cenv uses /opt/conda as default installation path of conda and
    /shared/conda/envs as installation path for conda-environments.
    If you want to change this you have to create a .cenv.yml file in your
    home-folder with the following content (with your path-definitions):

      1 ┃ conda_folder: /opt/conda
      2 ┃ env_folder: /shared/conda/envs



  1.2 Example for a valid meta.yaml used by cenv

      1 ┃ {% set data = load_setup_py_data() %}
      2 ┃
      3 ┃ package:
      4 ┃     name: "example_package"
      5 ┃     version: {{ data.get("version") }}
      6 ┃
      7 ┃ source:
      8 ┃     path: ..
      9 ┃
     10 ┃ build:
     11 ┃     build: {{ environ.get('GIT_DESCRIBE_NUMBER', 0) }}
     12 ┃     preserve_egg_dir: True
     13 ┃     script: python -m pip install --no-deps --ignore-installed .
     14 ┃
     15 ┃ requirements:
     16 ┃     build:
     17 ┃       - python 3.6.8
     18 ┃       - pip
     19 ┃       - setuptools
     20 ┃     run:
     21 ┃       - python 3.6.8
     22 ┃       - attrs >=18.2
     23 ┃       - jinja2 >=2.10
     24 ┃       - ruamel.yaml >=0.15.23
     25 ┃       - six >=1.12.0
     26 ┃       - yaml >=0.1.7
     27 ┃       - marshmallow >=3.0.0rc1*
     28 ┃
     29 ┃ test:
     30 ┃     imports:
     31 ┃         - example_package
     32 ┃
     33 ┃ extra:
     34 ┃     env_name: example


    ┃ "ATTENTION:Ensure to add a version to each package in requirements-run-section.
    ┃ Otherwise cenv will not update the projects conda-environment not defining
    ┃ a version for a package is defined to be an invalid meta.yaml-definiton."
```


#### search packages with cenv
Another possibility to run **cenv** is to search for the availability of
a package as a conda-package.
As default conda has the `conda search`-command.
Sometimes a python-package is not available from the conda-channels but as a
pypi-package.
This package can be converted to a conda-package as described before in this
documentation.
Using `cenv -s <package_name>` searches for availability of the passed
**package_name** in the conda-channels.
If the package is not available in these channels, it runs `pip search`.
If the package is found at pypi the required commands
`conda skeleton pypi ...` and `conda build ..` to convert the package to
a conda-package are shown.

Example for ``cenv -s`` with a package available on the conda-channels:
`cenv -s numpy`.

This returns:
```bash
┃ Search results for availability of conda-package numpy
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Loading channels: ...working... done
# Name                       Version           Build  Channel
numpy                          1.5.1          py26_1  pkgs/free
numpy                          1.5.1          py26_3  pkgs/free
numpy                          1.5.1          py26_4  pkgs/free
numpy                          1.5.1          py26_6  pkgs/free
numpy                          1.5.1        py26_ce0  pkgs/free
numpy                          1.5.1          py27_1  pkgs/free
numpy                          1.5.1          py27_3  pkgs/free
...
numpy                         1.15.4  py36h1d66e8a_0  pkgs/main
numpy                         1.15.4  py36h7e9f1db_0  pkgs/main
numpy                         1.15.4  py36h99e49ec_0  pkgs/main
numpy                         1.15.4 py37_blas_openblash1522bff_1000  conda-forge
numpy                         1.15.4 py37_blas_openblashb06ca3d_0  conda-forge
numpy                         1.15.4  py37h1d66e8a_0  pkgs/main
numpy                         1.15.4  py37h7e9f1db_0  pkgs/main
numpy                         1.15.4  py37h99e49ec_0  pkgs/main
numpy                         1.16.0 py27_blas_openblash1522bff_1000  conda-forge
numpy                         1.16.0 py36_blas_openblash1522bff_1000  conda-forge
numpy                         1.16.0 py37_blas_openblash1522bff_1000  conda-forge
numpy                         1.16.1 py27_blas_openblash1522bff_0  conda-forge
numpy                         1.16.1 py36_blas_openblash1522bff_0  conda-forge
numpy                         1.16.1 py37_blas_openblash1522bff_0  conda-forge
```

Another example for a package not available on the conda-channels but on pypi:
`cenv -s igoogle`.

This returns:
```bash
┃ Search results for availability of conda-package igoogle
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
igoogle (0.0.1)                - innovata-sambong-google
listparser (0.18)              - Parse OPML, FOAF, and iGoogle subscription lists.
cubicweb-portlets (0.3.0)      - cube to build portals like iGoogle or Netvibes
Products.Mlango (2.2.1)        - Mlango is a desktop dashboard implementation similar to the Google dashboard (iGoogle).
collective.idashboard (1.2.5)  - This Plone add-on product gives your dashboard features similiar to those of the iGoogle dashboard.
TracGViz (1.4.2)               - Trac Data Sources for Google Visualization API. Embed iGoogle gadgets using WikiFormatting.
igoogle not available in current channels.
The printed packages are results of the availability of this package in pip.
You'll have to convert the package to a conda-package yourself.
Create the required skeleton with:
    conda skeleton pypi igoogle --python=[DESIRED_PYTHON_VERSION]
Then build the package using:
    conda build igoogle --python=[DESIRED_PYTHON_VERSION]
```
If a package is neither found on conda-channels, nor pypi:
```bash
cenv -s blablub
```

The search results in the following:
```bash
┃ Search results for availability of conda-package blablub
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Package blablub neither available as conda-package, nor pypi-package.
```