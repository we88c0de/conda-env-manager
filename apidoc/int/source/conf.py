import os
import sys
import datetime as dt
import pytz
sys.path.insert(0, os.path.abspath('../../..'))


project = 'conda-env-manager'
copyright = '2019, Simon Kallfass'
author = 'Simon Kallfass'
version = ''
release = '2019'

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    ]

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'conda'
language = None
exclude_patterns = []
pygments_style = None
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
htmlhelp_basename = 'cenvdoc'
html_logo = '_static/logo.png'
html_title = 'conda-env-manager'
html_short_title = 'conda-env-manager'
html_last_updated_fmt = dt.datetime.utcnow().replace(tzinfo=pytz.utc).isoformat()

html_theme_options = {
    'canonical_url': 'http://www.ouroboros.info/',
    'analytics_id': '',
    'logo_only': True,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    # Toc options
    'collapse_navigation': False,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False}

html_show_sourcelink = False

napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_include_init_with_doc = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_examples = True
napoleon_use_param = True
napoleon_use_keyword = True
napoleon_use_rtype = False
napoleon_use_ivar = True
